from django.conf.urls import patterns, url

from mm_transactions import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<transaction_id>\d+)/$', views.detail, name='detail'),
)