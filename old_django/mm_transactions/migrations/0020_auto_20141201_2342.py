# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mm_transactions', '0019_auto_20141201_2338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 21, 42, 26, 943630, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 21, 42, 26, 943653, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 21, 42, 26, 942091, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 21, 42, 26, 942118, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
