# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mm_transactions', '0010_auto_20141201_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 33, 49, 194687, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 33, 49, 194714, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 33, 49, 192603, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 33, 49, 192633, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
