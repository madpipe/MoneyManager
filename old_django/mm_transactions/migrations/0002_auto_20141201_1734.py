# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mm_transactions', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('added_time', models.DateTimeField(default=datetime.datetime(2014, 12, 1, 15, 34, 29, 898701, tzinfo=utc))),
                ('modified_time', models.DateTimeField(default=datetime.datetime(2014, 12, 1, 15, 34, 29, 898728, tzinfo=utc))),
                ('deleted_time', models.DateTimeField()),
                ('amount', models.FloatField()),
                ('description', models.CharField(max_length=500)),
                ('attachment', models.CharField(default='', max_length=500)),
                ('category', models.ForeignKey(to='mm_transactions.Category')),
                ('subcategory', models.ForeignKey(to='mm_transactions.Subcategory')),
                ('wallet', models.ForeignKey(to='mm_transactions.Wallet')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='treansaction',
            name='category',
        ),
        migrations.RemoveField(
            model_name='treansaction',
            name='subcategory',
        ),
        migrations.RemoveField(
            model_name='treansaction',
            name='wallet',
        ),
        migrations.DeleteModel(
            name='Treansaction',
        ),
        migrations.AlterField(
            model_name='wallet',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 15, 34, 29, 897346, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 15, 34, 29, 897374, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
