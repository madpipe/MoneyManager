# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mm_transactions', '0004_auto_20141201_2052'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction',
            old_name='attachment',
            new_name='attachment_file_path',
        ),
        migrations.AlterField(
            model_name='transaction',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 18, 55, 57, 654006, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 18, 55, 57, 654033, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 18, 55, 57, 652525, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 18, 55, 57, 652552, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
