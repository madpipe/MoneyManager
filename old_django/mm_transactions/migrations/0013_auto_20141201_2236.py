# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('mm_transactions', '0012_auto_20141201_2234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 36, 10, 516899, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='attachment_file_path',
            field=models.CharField(max_length=500, null=True, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 36, 10, 516924, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 36, 10, 515212, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 36, 10, 515243, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
