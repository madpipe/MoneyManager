# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('mm_transactions', '0006_auto_20141201_2152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='description',
            field=models.CharField(max_length=200, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=100, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='subcategory',
            name='description',
            field=models.CharField(max_length=200, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='subcategory',
            name='name',
            field=models.CharField(max_length=100, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 13, 18, 360820, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='amount',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='deleted_time',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='description',
            field=models.CharField(max_length=500, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 13, 18, 360842, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='added_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 13, 18, 359348, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='deleted_time',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='description',
            field=models.CharField(max_length=200, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='modified_time',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 1, 20, 13, 18, 359375, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='name',
            field=models.CharField(max_length=100, default=''),
            preserve_default=True,
        ),
    ]
