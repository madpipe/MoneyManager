# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subcategory',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=200)),
                ('category', models.ForeignKey(to='mm_transactions.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Treansaction',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('added_time', models.DateTimeField(default=datetime.datetime(2014, 12, 1, 15, 34, 15, 933065, tzinfo=utc))),
                ('modified_time', models.DateTimeField(default=datetime.datetime(2014, 12, 1, 15, 34, 15, 933093, tzinfo=utc))),
                ('deleted_time', models.DateTimeField()),
                ('amount', models.FloatField()),
                ('description', models.CharField(max_length=500)),
                ('attachment', models.CharField(max_length=500, default='')),
                ('category', models.ForeignKey(to='mm_transactions.Category')),
                ('subcategory', models.ForeignKey(to='mm_transactions.Subcategory')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Wallet',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=200)),
                ('added_time', models.DateTimeField(default=datetime.datetime(2014, 12, 1, 15, 34, 15, 931733, tzinfo=utc))),
                ('modified_time', models.DateTimeField(default=datetime.datetime(2014, 12, 1, 15, 34, 15, 931761, tzinfo=utc))),
                ('deleted_time', models.DateTimeField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='treansaction',
            name='wallet',
            field=models.ForeignKey(to='mm_transactions.Wallet'),
            preserve_default=True,
        ),
    ]
