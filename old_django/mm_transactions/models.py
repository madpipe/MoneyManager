from django.db import models
from django.utils import timezone

# Create your models here.
class Wallet(models.Model):
    name = models.CharField(max_length=100, default="")
    description = models.CharField(max_length=200, default="")
    added_time = models.DateTimeField(default=timezone.now())
    modified_time = models.DateTimeField(default=timezone.now())
    deleted_time = models.DateTimeField(null=True, blank=True)
    def __str__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=100, default="")
    description = models.CharField(max_length=200, default="")
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Categories"

class Subcategory(models.Model):
    name = models.CharField(max_length=100, default="")
    description = models.CharField(max_length=200, default="")
    category = models.ForeignKey(Category)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Subcategories"

class Transaction(models.Model):
    #user = models.ForeignKey(User)
    amount = models.FloatField(default=0)
    description = models.CharField(max_length=500, default="")
    wallet = models.ForeignKey(Wallet)
    category = models.ForeignKey(Category)
    subcategory = models.ForeignKey(Subcategory)
    attachment_file_path = models.CharField(max_length=500, default="", blank=True)
    added_time = models.DateTimeField(default=timezone.now())
    modified_time = models.DateTimeField(default=timezone.now())
    deleted_time = models.DateTimeField(null=True, blank=True)
