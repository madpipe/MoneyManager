from django.http import HttpResponse
from django.template import RequestContext, loader
from mm_transactions.models import Transaction

def index(request):
  all_transactions_list = Transaction.objects.order_by('-added_time')
  template = loader.get_template('mm_transactions/index.html')
  context = RequestContext(request, {
            'all_transactions_list': all_transactions_list,
      })
  return HttpResponse(template.render(context))

def detail(request, transaction_id):
    return HttpResponse("Transaction %s." % transaction_id)