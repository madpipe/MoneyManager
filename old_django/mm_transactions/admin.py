from django.contrib import admin
from mm_transactions.models import *

# Register your models here.
admin.site.register(Wallet)
admin.site.register(Category)
class SubcategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'category')
admin.site.register(Subcategory, SubcategoryAdmin)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('amount', 'description', 'category', 'subcategory', 'wallet', 'added_time')
    list_filter = ('category', 'subcategory', 'wallet', 'added_time')
    search_fields = ('description', 'amount')
admin.site.register(Transaction, TransactionAdmin)
