Money Manager
==============

My project to keep track of my money flow using the python Django framework.

###This project is developed using:
* Chromium
* Arch Linux
* Kubuntu
* KDevelop
* Python 3
* Django 1.7
* SQLite 3

####Admin credentials
User: admin

email: admin@something.org

password: admin123

###How to Use with Docker
Run the container with: docker run --rm -p 8000:8000 ovidiub13/moneymanager

You can find more info about the image here: https://hub.docker.com/r/ovidiub13/moneymanager/
